package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SALARIE")
public class Salarie implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name="IDSALARIE")
	private Long id;
	@Column(name="NOMSALARIE")
	private String nom;
	@Column(name="PRENOMSALARIE")
	private String prenom;
	@Column(name="NUMSECUSALARIE")
	private int numSecu;
	
	@ManyToOne
	@JoinColumn(name="employeur_id")
	private Employeur employeur;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNumSecu() {
		return numSecu;
	}

	public void setNumSecu(int numSecu) {
		this.numSecu = numSecu;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	
	
	
	
	
	public Salarie() {
		super();
	}

	public Salarie(String nom, String prenom, int numSecu, Employeur employeur) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numSecu = numSecu;
		this.employeur = employeur;
	}
	
	
}
