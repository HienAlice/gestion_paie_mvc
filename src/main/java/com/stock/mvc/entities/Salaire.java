package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

  @Entity
  
  @Table(name="SALAIRE") public class Salaire implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY) 
  private Long idSalaire;
  private BigDecimal netSalaire;
  private BigDecimal SalaireNet;
  private String congesPayes;
  
  @Column(name="idEmployeur", nullable=false)
  private Long idEmployeur;
  
  
  }
 