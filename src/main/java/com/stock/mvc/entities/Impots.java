package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="IMPOTS") 
public class Impots implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name="IDIMPOTS")
	private Long idImpots;
	private BigDecimal netAvantImpot;
	private BigDecimal totalVerserEmployeur;
	private BigDecimal baseImpot;
	private BigDecimal tauxPersonnalise;
	private BigDecimal montantPrelevImpot;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "impots_idSalaire", unique = true)
	private Salaire salaire;

	public Long getIdImpots() {
		return idImpots;
	}

	public void setIdImpots(Long idImpots) {
		this.idImpots = idImpots;
	}

	public BigDecimal getNetAvantImpot() {
		return netAvantImpot;
	}

	public void setNetAvantImpot(BigDecimal netAvantImpot) {
		this.netAvantImpot = netAvantImpot;
	}

	public BigDecimal getTotalVerserEmployeur() {
		return totalVerserEmployeur;
	}

	public void setTotalVerserEmployeur(BigDecimal totalVerserEmployeur) {
		this.totalVerserEmployeur = totalVerserEmployeur;
	}

	public BigDecimal getBaseImpot() {
		return baseImpot;
	}

	public void setBaseImpot(BigDecimal baseImpot) {
		this.baseImpot = baseImpot;
	}

	public BigDecimal getTauxPersonnalise() {
		return tauxPersonnalise;
	}

	public void setTauxPersonnalise(BigDecimal tauxPersonnalise) {
		this.tauxPersonnalise = tauxPersonnalise;
	}

	public BigDecimal getMontantPrelevImpot() {
		return montantPrelevImpot;
	}

	public void setMontantPrelevImpot(BigDecimal montantPrelevImpot) {
		this.montantPrelevImpot = montantPrelevImpot;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}

	
	
	
	public Impots() {
		super();
	}

	public Impots(BigDecimal netAvantImpot, BigDecimal totalVerserEmployeur, BigDecimal baseImpot,
			BigDecimal tauxPersonnalise, BigDecimal montantPrelevImpot, Salaire salaire) {
		super();
		this.netAvantImpot = netAvantImpot;
		this.totalVerserEmployeur = totalVerserEmployeur;
		this.baseImpot = baseImpot;
		this.tauxPersonnalise = tauxPersonnalise;
		this.montantPrelevImpot = montantPrelevImpot;
		this.salaire = salaire;
	}
	
	
	
	
	

}
