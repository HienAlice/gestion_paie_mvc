package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEUR")
public class Employeur implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name="IDEMPLOYEUR")
	private Long idEmployeur;
	@Column(name="RAISONSOCIALE")
	private String raisonsociale;
	@Column(name="SIRET")
	private int siret;
	@Column(name="CODEAPE")
	private int codeAPE;
	
	
	@OneToMany(mappedBy="idEmployeur")
	private List<Salaire> salaire;
	
	
	public Long getIdEmployeur() {
		return idEmployeur;
	}


	public void setIdEmployeur(Long idEmployeur) {
		this.idEmployeur = idEmployeur;
	}


	public String getRaisonsociale() {
		return raisonsociale;
	}


	public void setRaisonsociale(String raisonsociale) {
		this.raisonsociale = raisonsociale;
	}


	public int getSiret() {
		return siret;
	}


	public void setSiret(int siret) {
		this.siret = siret;
	}


	public int getCodeAPE() {
		return codeAPE;
	}


	public void setCodeAPE(int codeAPE) {
		this.codeAPE = codeAPE;
	}


	public List<Salaire> getSalaire() {
		return salaire;
	}


	public void setSalaire(List<Salaire> salaire) {
		this.salaire = salaire;
	}


	public Employeur(String raisonsociale, int siret, int codeAPE, List<Salaire> salaire) {
		super();
		this.raisonsociale = raisonsociale;
		this.siret = siret;
		this.codeAPE = codeAPE;
		this.salaire = salaire;
	}


	public Employeur() {
		super();
	}
	
	
}
