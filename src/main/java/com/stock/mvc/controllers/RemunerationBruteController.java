package com.stock.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/remunerationbrute", method = RequestMethod.GET)
public class RemunerationBruteController {
	@RequestMapping(value="")
	public String salarie() {
		return "remunerationbrute/remunerationbrute";
	}
}
