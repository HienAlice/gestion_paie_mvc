package com.stock.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/impots", method = RequestMethod.GET)
public class ImpotsController {
	@RequestMapping(value="")
	public String salarie() {
		return "impots/impots";
	}
}
