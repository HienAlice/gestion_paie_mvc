package com.stock.mvc.dao;

import com.stock.mvc.entities.Salaire;

public interface ISalaireDao extends IGenericDao<Salaire> {

}
