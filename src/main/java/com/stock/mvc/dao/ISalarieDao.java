package com.stock.mvc.dao;

import com.stock.mvc.entities.Salarie;

public interface ISalarieDao extends IGenericDao<Salarie> {

}
