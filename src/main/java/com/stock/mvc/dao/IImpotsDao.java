package com.stock.mvc.dao;

import com.stock.mvc.entities.Impots;

public interface IImpotsDao extends IGenericDao<Impots> {

}
