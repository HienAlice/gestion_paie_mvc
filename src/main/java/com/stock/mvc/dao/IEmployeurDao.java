package com.stock.mvc.dao;

import com.stock.mvc.entities.Employeur;

public interface IEmployeurDao extends IGenericDao<Employeur>{

}
