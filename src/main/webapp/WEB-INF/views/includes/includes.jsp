<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ page session="false" %>
<%@ page trimDirectiveWhitespaces="true" %>

<fmt:setLocale value="fr_FR"/>
<fmt:setBundle basename="com.stock.mvc.i18n.applicationresources_fr_FR"/>